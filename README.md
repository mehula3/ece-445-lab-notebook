# ECE 445 Lab Notebook


## 3rd Feb 2023
Objectives: Work on Project Proposal

Due to the initial project idea submission we had to do on the web board, we already had a good broad overview of what product we were developing and what problem we were solving. The first thing we decided was what would be our high level requirements. When creating our high level requirements, we examined what would be the most important things the glove would need to do in order to successfully solve the problem we were solving; this is how we came up with our high level requirements. 

The next thing we did was figure out how we were going to build the glove circuitry and so we created the block diagram for our project. While creating the block diagram, we needed to do a bit of a deeper dive into our subsystems and what these were actually going to look like, and how they were actually going to function. We first looked into the sensor subsystem, and we immediately knew we wanted to use an IMU to track hand movements. Then we also needed something to track finger movements (bends), and we had 2 ideas: flex sensors or hall effect sensors with small magnets mounted on the knuckle of each finger. In the end we decided to use the flex sensors because these would make the mechanical construction of the glove easiser, and it will probably be much easier to collect and interpret data from the flex sensors than the hall effect sensors. 

Then, we had already decided on using the ESP32 because it had a lot of documentation, was easy to use and interface with, and had an in-built bluetooth module so that we could easily execute the bluetooth functionality of our project. With the power subsystem, we wanted a rechargeable Li-ion battery. Due to this battery's nominal voltage, we would also need a voltage regulator to supply 3.3V for the rest of the circuit. We also added a USB connection so that the battery could be charged. In order to charge the battery, we also knew we needed a battery charging IC to supply the constant current and voltage to charge the battery. 

![image info](proposal_block_diagram.jpg)

## 7th Feb 2023
Objectives: Finish and submit Project Proposal

During this session, we needed to determine the requirements for all of our subsystems. For the sensor subsystem, when thinking about requirements, we needed to be able to easily collect data from the sensors. For the control subsystem, when thinking about requirements, we were focussing on things like making sure the MC could analayse all the data and send bluetooth commands to a phone (we found out that there were protocols and libraries for this). For the power subsystem, when thinking about requirements, we were focussing on achieving battery life needs, safe voltage and current draws, and being able to charge the battery. For the output subsystem, we just need to be able to turn the vibration motor on and off from the MC. 

Also, my teammate Oliver conducted the tolerance analysis to determine the max current draw of our entire circuit so that we knew what current our battery would need to supply. The way he found the current draws for each component was by looking at each component's datasheet and finding the max current draw values. This analysis also helped us determine the capacity of battery we would need. Since we wanted the battery to last at least 3 hours, after knowing the max current draw was approximately 800mA, we were able to calculate that we would need a battery capacity of at least 2.4Ah (3h * 0.8A). We decided to go with a 3Ah battery to have a bit of breathing room above our requirement. 

We finished and submitted the proposal today.

## 10th Feb 2023
Objectives: Develop PCB circuit schematic 

Today, we developed our circuit schematic. With the flex sensors, we knew we had to create a voltage divider circuit so that the MC could read the voltage drop across the shunt resistor. With the IMU, we just needed to connect the SDA and SCL I2C lines to the MC. We also looked at the datasheet in order to find things such as needing the pull up resistors on the SCL, SDA, and CS as well as driving VDDIO high with a capacitor connected to ground. With the MC, the readings from the flex sensor circuit had to be connected to ADC pins (which were found in the datasheeet), and the SDA and SCL lines needed to connect to the IMU. The datasheet also showed us how to drive the VDD and EN lines high as seen in the schematic. With the vibration motor, we created a common emitter amplifier circuit with a BJT to amplify the current from the MC to the vibration motor so that we can get a high level of vibration from the motor. We also included a capacitor in parallel with the vibration motor in order to limit any voltage spikes, and a diode in in parallel with the vibration motor to stop any reverse current. 

Power Subsytem: The battery is connected to the voltage regulator which has capacitors on its input and output to better control the steadiness of the signal, as specified in the datasheet. We have a USB connector in order to charge the battery. The power line of the USB connects to the battery IC. We have placed capcitors and resistors on the battery IC pins as specified in the datasheet. The resistor R7 allows us to control the constant current supplied by the battery IC to the battery to charge it. The battery IC provides a constant voltage source of 4.2V and provides a variable constant current set with resistor R7 which we have set to 500mA. For the battery, to ensure a safe fast charge current you need a current of less than 0.5*C mA. For the battery we are using, C = 3000mA, so the max charge current is 1500mA. Thus, the battery IC supplying 500mA of current to charge will be safe. 

![image info](Screen_Shot_2023-02-17_at_5.28.53_PM.png)

Additionally, when creating the flex sensor voltage divider circuit, we wanted to make sure that we were receiving a good voltage range of values across the shunt resistor which the MC could easily measure. Thus, in order to find the right shunt resistor value, we did the calculations below to verify. We said that a good voltage range is from about 0.5V to 2.5V, and so from these values and the resistance of the flex sensor at its unflexed and flexed states, allowed us to determine (by trial and erorr) a good shunt resistor value. 

![image info](Screen_Shot_2023-05-04_at_10.58.37_PM.png)

## 14th Feb 2023
Objectives: Make some changes to circuit schematic and start work on design document

We realised we made a mistake in our circuit because we don't have any way to program the MC. So, we added a USB to UART bridge to get the RX and TX lines which you need to program the MC. We also added Reset and GPIO0 buttons so that we could power cycle the MC and allow the MC to enter bootloader mode, respectively. This also involved building the transistor feedback loop with the RS232 signals (DTR and RTS), which we found in the datasheet for the MC. 

![image info](Screen_Shot_2023-02-21_at_4.29.20_PM.png)

We also started working on the design document and adding all of the descriptions for our subsystems based on the circuit schematic. We also developed our project schedule based on assignment deadlines in the class as well as how much time we believed we would need to complete certain activities such as testing and iterative design. 

## 21th Feb 2023
Objectives: Complete Design Document

With the circuit schematic completed last time, we just had to find all of our parts and create and subsystem requirements and verifications. We created a list of all of our parts and created our cost estimate. Additionally, we added requirements and verifications for each subsystem so that we could test those things in order to make sure the circuit was working correctly. 

We submitted the Design Document.

## 24th Feb 2023
Objectives: Team Contract and Design Review Prep

We completed the team contract and reviewed our whole design to be ready for hte Design Review. \

We also ordered our major parts so that we could start unit testing the circuit. We ordered development boards for things such as our MC so that we could test the functionality of our design on a breadboard. 

## 28th Feb 2023
Objectives: Start unit testing circuit subsystems

We started testing the vibration motor circuit. We wanted to make sure that we could easily switch on the BJT and that we could turn on the vibration motor with a high level of vibration. After a bit of testing we had to change the location of our resistors and change the resistor values to get a higher current through the vibration motor. We went from the first circuit to the second circuit, shown below. 

![image info](IMG_1737.jpg)

![image info](IMG_1739.jpg)


## 3rd March 2023

We unit tested the battery IC circuit. We were happy to find that the battery was successfully being charged from the battery IC.

We also updated the circuit schematic for the vibration motor circuit based on our testing last time. For the USB-to-UART bridge we were able to find a breaout board which we decided to use. Also, for the IMU we changed it to a connector because we got a IMU pinout board because the IMU is extremely small and difficult to solder onto a PCB. 

![image info](Screen_Shot_2023-04-11_at_3.57.51_PM.png)

We also created the PCB layout and submitted the PCB for order. For the first iteration, we decided to just use through hole mounting for all the passive components. 

![image info](Screen_Shot_2023-05-04_at_11.50.47_PM.png)

## 7th March 2023

We started testing our flex sensors. We setup our software environment for programming the MC. We used the MC development board and created the flex sensor voltage divider circuit on a breadboard. The main thing we were looking for was to get a big reading change between unflexed and flexed. However, the change was not as big as expected, so we changed our resistor from 4.7k ohms to 2.2k ohms and got much better readings. 

## 21st March 2023

We updated the PCB to use surface mount passive components to reduce the PCB size. 

![image info](PNG_image.jpg)


## 28th March 2023
Objectives: Testing entire PCB

A big problem was that when the USB was connected, the battery was charging and discharging at the same time. So we had to add a MOSFET switching circuit so that either the USB or the battery powered the circuit at one time and so that wehn the USB was plugged in, the battery would only be charged and not discharge, since the USB was powering the rest of the circuit. 

![image info](Screen_Shot_2023-04-04_at_8.23.53_PM.png)

## 4th April 2023

Started developing 3D printed encasing for PCB.

Started implementing software for gestue recognition. This requried a lot of tuning for flex sensor thresholding values. 

## 7th April 2023

Implemented acceleration thresholding for gesture recogniton. Faced issues of getting lots of false commands and so had to do a lot of trial and error to find the right thresholding values. 

## 11th April 2023

Fully tested latest PCB with switching circuit implemented. Everything works correctly. The battery charging circuit works exactly as we designed it to work. 

## 14th April 2023

Started working on implementing an extremely well-polished, clean design of the glove by reducing PCB enclosure size and changing the mounting to make the glove more comfortable. 














